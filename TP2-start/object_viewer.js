var gl;
var canvas;
var program;


var mModelView;
var mModelViewLoc;
var mProjectionLoc;

var mProjection;

var primSelected = 3;
var viewSelected = 0;

window.onload = function init() {
    // Get the canvas
    canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if(!gl) { alert("WebGL isn't available"); }

    // Setup the contexts and the program
    gl = WebGLUtils.setupWebGL(canvas);
    program = initShaders(gl, "vertex-shader", "fragment-shader");

    mModelViewLoc = gl.getUniformLocation(program, "mModelView");
    mProjectionLoc = gl.getUniformLocation(program, "mProjection");

    gl.clearColor(0.3, 0.3, 0.3, 1.0);
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    gl.viewport(0,0,canvas.width, canvas.height);

    sphereInit(gl);
    cubeInit(gl);
    pyramidInit(gl);

    window.onresize = function() {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;

        aspect = canvas.width / canvas.height;
    }

    render();
}

function drawObject(gl, program)
{
    if(primSelected == 0) cubeDrawWireFrame(gl,program);
    if(primSelected == 1) sphereDrawWireFrame(gl, program);
    if(primSelected == 3) pyramidDrawWireFrame(gl,program);
    // cubeDrawWireFrame(gl,program);
}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    // Top view
    var at = [0, 0, 0];
    var eye = [0, 0, 1];
    var up = [0, 1, 0];
    var mModelView = lookAt(eye, at, up);

    var topView = mult(mModelView, rotateX(90)); // Rotate X
    mProjection = ortho(-1, 1, -1, 1, 0, 1);//  mat4();
    gl.uniformMatrix4fv(mProjectionLoc, false, flatten(mProjection));

    gl.viewport(0, 0, canvas.width / 2, canvas.height / 2);
    gl.uniformMatrix4fv(mModelViewLoc, false, flatten(topView));
    drawObject(gl, program);

    // Front view

    var frontView = mult(mModelView, rotateX(0)); // Rotate X

    gl.viewport(0, canvas.height / 2, canvas.width / 2, canvas.height / 2);
    gl.uniformMatrix4fv(mModelViewLoc, false, flatten(frontView));
    drawObject(gl, program);

    // Side view

    var sideView = mult(mModelView, rotateY(90)); // Rotate X

    gl.viewport(canvas.width / 2, canvas.height / 2, canvas.width / 2, canvas.height / 2);
    gl.uniformMatrix4fv(mModelViewLoc, false, flatten(sideView));
    drawObject(gl, program);


    // Other view

    var afterRotation;
    if (viewSelected == 0) {
        at = [0, 0, 1];
        eye = [0, 0, 0];
        up = [0, 1, 0];
        mModelView = lookAt(eye, at, up);

        afterRotation = mult(mModelView, rotateX(0)); // Rotate X
        afterRotation = mult(afterRotation, rotateY(0)); // Rotate Y
        afterRotation = mult(afterRotation, rotateZ(0)); // Rotate Z

        mProjection = obliqueLecture(1, 45);

    } else if (viewSelected == 1) {
        at = [0, 0, 0];
        eye = [0, 0, 0];
        up = [0, 1, 0];
        mModelView = lookAt(eye, at, up);

        afterRotation = mModelView;
        // afterRotation = mult(mModelView,rotateX(0) ); // Rotate X
        // afterRotation = mult(afterRotation, rotateY(42)); // Rotate Y
        // afterRotation = mult(afterRotation, rotateZ(0)); // Rotate Z

        mProjection = ortho(-1, 1, -1, 1, 100, -100);//  mat4();
        mProjection = mult(mProjection, rotateX(7));
        mProjection = mult(mProjection, rotateY(42));
    } else if (viewSelected == 2) {
        at = [0, 0, 0];
        eye = [0, 0, -1];
        up = [0, 1, 0];
        mModelView = lookAt(eye, at, up);

        afterRotation = mModelView;

        mProjection = perspective(90, gl.canvas.clientWidth/ gl.canvas.clientHeight, 0, 2000);
    }

   mModelView =  afterRotation;

    gl.uniformMatrix4fv(mProjectionLoc, false, flatten(mProjection));


    gl.viewport(canvas.width/2,0,canvas.width/2, canvas.height/2);
    gl.uniformMatrix4fv(mModelViewLoc, false, flatten(mModelView));
    drawObject(gl, program);

    window.requestAnimationFrame(render);
}


function primitiveSelect() {
    var x = document.getElementById("primSelect");
    var i = x.selectedIndex;
    var optionSelected = x.options[i].text;
    switch (optionSelected){
        case "Cube": primSelected = 0; break;
        case "Sphere": primSelected = 1; break;
        case "Donut": primSelected = 2; break;
        case "Pyramid": primSelected = 3; break;
        default: primSelected = 0; break;
    }
}

function viewSelect() {
    var x = document.getElementById("viewSelect");
    var i = x.selectedIndex;
    viewSelected = x.options[i].value;
}


function oblique( matrix ,theta, phi )
{
    function degreesToRadians(degrees){
        return degrees * Math.PI / 180;
    }
    var t = degreesToRadians(theta);
    var p = degreesToRadians(phi);
    var cotT = -1/Math.tan(t);
    var cotP = -1/Math.tan(p);

    var result =  matrix; //mat4();
    result[0][0] = 1;
    result[0][2] = cotT;
    result[1][1] = 1;
    result[1][2] = cotP;
    result[2][2] = 1;
    result[3][3] = 1;

    transpose(result);
    return result;
}

function obliqueLecture( l, theta) {
    var cosT = -1 * l * Math.cos(radians( theta));
    var sinT = -1 * l * Math.sin(radians(theta));

    var result = mat4();
    result[0][0] = 1;
    result[0][2] = cosT;
    result[1][1] = 1;
    result[1][2] = sinT;
    result[3][3] = 1;

    return result;
}
